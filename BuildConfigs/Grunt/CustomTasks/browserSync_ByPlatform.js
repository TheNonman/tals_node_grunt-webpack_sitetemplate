
module.exports = function (grunt)
{
	const os = require('os');

	grunt.registerMultiTask('browserSync_ByPlatform', 'Detect OS and run different browserSync task based on it', function() {
		grunt.log.writeln('Platform: ' + os.platform());

		if (os.platform().startsWith('win')) {
			if (this.target === 'all') {
				grunt.task.run( ['browserSync:dev_win_all'] );
			}
			else {
				grunt.task.run( ['browserSync:dev_win'] );
			}
		}
		else if (os.platform().startsWith('darwin')) { // This is what Grunt returns for OSX
			if (this.target === 'all') {
				grunt.task.run( ['browserSync:dev_mac_all'] );
			}
			else {
				grunt.task.run( ['browserSync:dev_mac'] );
			}
		}
		else {
			if (this.target === 'all') {
				grunt.task.run( ['browserSync:dev_nix_all'] );
			}
			else {
				grunt.task.run( ['browserSync:dev_nix'] );
			}
		}
	});
};
