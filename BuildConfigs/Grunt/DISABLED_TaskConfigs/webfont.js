module.exports = function (grunt, options)
{
	return {
		icons: {
			src: '<%= BuildConfigs.src.fonts_to_build %>/**/*.svg',
			dest: '<%= BuildConfigs.temp.fonts_built %>',
			destScss: '<%= BuildConfigs.temp.css_pre_source %>/_GENERATED_SCSS',
			options: {
				font: 'testFont',
				hashes: true,
				styles: 'font,icon',
				types: 'eot,woff2,woff,ttf,svg',
				order: 'eot,woff,ttf,svg',
				syntax: 'bem',
				template: '',
				templateOptions: {},
				stylesheets: ['scss'],
				relativeFontPath: '/fonts',
				fontPathVariables: false,
				version: ''
			}
		}
	}
};
