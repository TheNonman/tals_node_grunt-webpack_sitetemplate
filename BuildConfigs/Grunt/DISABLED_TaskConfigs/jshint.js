module.exports = function (grunt, options)
{
	return {
		sourceDebug: {
			src: ['gruntfile.js',
				'<%= BuildConfigs.src.js %>/**/*.js',
				'!<%= BuildConfigs.src.js_3P %>/**/*.js'
			],
			options: options.JSHintOptions.Debug
		},
		sourceProduction: {
			src: ['gruntfile.js',
				'<%= BuildConfigs.src.js %>/**/*.js'
			],
			options: options.JSHintOptions.Production
		},
		grunt: {
			src: ['gruntfile.js'],
			options: options.JSHintOptions.Production
		}
	}
};
