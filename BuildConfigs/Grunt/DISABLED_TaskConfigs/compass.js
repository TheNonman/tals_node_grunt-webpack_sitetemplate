module.exports = function (grunt, options)
{
	return {
		options: {
			sassDir: '<%= BuildConfigs.temp.css_pre_source %>',
			cssDir: '<%= BuildConfigs.temp.css %>',
			generatedImagesDir: 'img/__generated',
			imagesDir: 'img',
			javascriptsDir: 'js',
			fontsDir: 'fonts',
			httpImagesPath: 'img',
			httpGeneratedImagesPath: 'img/__generated',
			outputStyle: 'nested',
			relativeAssets: false,
			debugInfo: false,
			quiet: false,
			sourcemap: true
		},
		production: {
			options: {
				environment: 'production'
			}
		},
		test: {
			options: {
				environment: 'development'
			}
		}
	}
};
