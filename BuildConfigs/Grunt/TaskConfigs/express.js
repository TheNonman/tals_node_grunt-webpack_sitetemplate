module.exports = function (grunt, options)
{
	return {
		options: {
			logs: {
				out: '__BuildLogs/serverOutput.log',
				err: '__BuildLogs/serverErrors.log'
			}
		},
		dev: {
			options: {
				script: 'ServerConfigs/server_app.js'
			}
		},
		prod: {
			options: {
				script: 'ServerConfigs/server_app.js',
				node_env: 'production'
			}
		},
		test: {
			options: {
				script: 'ServerConfigs/server_app.js'
			}
		},
		webpack_dev: {
			options: {
				script: 'ServerConfigs/webpack_server_app.js'
			}
		}
	}
};
