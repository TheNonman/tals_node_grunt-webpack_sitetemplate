const sass = require('node-sass');

module.exports = function (grunt, options)
{
	return {
		options: {
			implementation: sass,
			sourceMap: true
		},
		dist: {
			files:
			{
				'<%= BuildConfigs.staging.css %>/main.css': '<%= BuildConfigs.src.css %>/app_main.scss'
			}
		}
	}
};
