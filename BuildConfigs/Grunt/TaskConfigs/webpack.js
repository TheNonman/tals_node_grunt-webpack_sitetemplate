

//
// Grunt Webpack Tasks
// =====================================
// Each Task/Webpack config needs to be wrapped in a function. This is both to prevent circular references from occuring
// and also to prevent the Webpack configs from being generated prior to actual use by Grunt.

const chalk = require('chalk');
const merge = require('webpack-merge');

//
//
// Webpack Plugins
// ================================================================================

// Custom
const WebpackFlowLogger = require('../../WebPack/CustomPlugins/WebpackFlowNotifier');
process.env.IS_GRUNT_MANAGED = true;

module.exports = function (grunt, options)
{
	return {
		options: {
			stats: !process.env.NODE_ENV || process.env.NODE_ENV === 'development',
			mode: 'development'
		},
		dev: function() {
			const commonConfig = require('../../WebPack/webpack.common.config.js');

			const winston = require('winston');

			let logger = null;
			if (winston.loggers.has("webpack")) {
				logger = winston.loggers.get("webpack");
				logger.info('Grunt Webpack Config Task Attached to Webpack Build Winston Logger');
			}
			else {
				console.log(chalk.red('Grunt Webpack Config Task FAILED to Attach Webpack Build Winston Logger !!!'));
			}

			const extraConfig = {
									plugins: [
										new WebpackFlowLogger({mainLogger: logger}),
									]
			};

			return merge(commonConfig, extraConfig);
		},
		dev_watch: function() {
			const commonConfig = require('../../WebPack/webpack.common.config.js');

			const winston = require('winston');

			let logger = null;
			if (winston.loggers.has("webpack")) {
				logger = winston.loggers.get("webpack");
				logger.info('Grunt Webpack Config Task Attached to Webpack Build Winston Logger');
			}
			else {
				console.log(chalk.red('Grunt Webpack Config Task FAILED to Attach Webpack Build Winston Logger !!!'));
			}

			const extraConfig = {
									watch: true,
									plugins: [
										new WebpackFlowLogger({mainLogger: logger}),
									]
			};

			return merge(commonConfig, extraConfig);
		},
		prod: function() {
			const commonConfig = require('../../WebPack/webpack.common.config.js');
			const extraConfig = {
									mode: 'production',
									output: {
										publicPath: '/'
									},
									devtool: 'source-map'
			};

			return merge(commonConfig, extraConfig);
		}
	}
};
