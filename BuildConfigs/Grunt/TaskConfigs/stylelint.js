
module.exports = function (grunt, options)
{
	return {
		css: {
			options: {
				configFile: 'BuildConfigs/Stylelint/css.extensions.config.js',
				formatter: 'string',
				ignoreDisables: false,
				failOnError: true,
				outputFile: '',
				reportNeedlessDisables: false
			},
			src: [
				'<%= BuildConfigs.src.css %>/**/*.css',
				'!<%= BuildConfigs.src.css %>/a_third_party/**'
			]
		},
		temp_css: {
			options: {
				configFile: 'BuildConfigs/Stylelint/css.extensions.config.js',
				formatter: 'string',
				ignoreDisables: false,
				failOnError: true,
				outputFile: '',
				reportNeedlessDisables: false
			},
			src: [
				'<%= BuildConfigs.temp.css %>/**/*.css',
				'!<%= BuildConfigs.temp.css %>/a_third_party/**'
			]
		},
		scss: {
			options: {
				configFile: 'BuildConfigs/Stylelint/base.scss.config.js',
				formatter: 'string',
				ignoreDisables: false,
				failOnError: true,
				outputFile: '',
				reportNeedlessDisables: false,
				syntax: 'scss'
			},
			src: [
				'<%= BuildConfigs.src.css %>/**/*.scss',
				'!<%= BuildConfigs.src.css %>/a_third_party/**'
			]
		},
		css_fail: {
			options: {
				configFile: 'BuildConfigs/Stylelint/css.extensions.config.js',
				formatter: 'string',
				ignoreDisables: false,
				failOnError: true,
				outputFile: '',
				reportNeedlessDisables: false
			},
			src: [
				'BuildConfigs/Stylelint/fail_tests/*.css'
			]
		},
		scss_fail: {
			options: {
				configFile: 'BuildConfigs/Stylelint/base.scss.config.js',
				formatter: 'string',
				ignoreDisables: false,
				failOnError: true,
				outputFile: '',
				reportNeedlessDisables: false,
				syntax: 'scss'
			},
			src: [
				'BuildConfigs/Stylelint/fail_tests/*.scss'
			]
		}
	}
}
