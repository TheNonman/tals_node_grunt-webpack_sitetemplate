/*

	Base Stylelint config, targeting SCSS files in particular as this file will be the config used by editor plug-ins.

	The stylelint grunt processes extend off of this config, overriding as appropriate.

	TODO :  Continue going through the main stylelint rule options starting with:
			selector-list-comma-newline-after

	TODO :  Go through the rule options provided by stylelint-scss

*/

module.exports =
{
	"plugins": [
		"stylelint-scss"
	],
	"rules": {
		//
		//
		//  Basic Validation Rules
		//  Validate that the CSS is in Fact Valid CSS
		// ====================================================================
		// ====================================================================

		"scss/at-rule-no-unknown": [ true,
			// Validates :  At-rules defined in the CSS Specifications (i.e. @media, @charset, etc.)
						{	"severity": "error",
							"ignoreAtRules": [] // Add Valid, but Non-Standard At-rules to this Array
			}
		],
		"color-no-invalid-hex": [ true,
						{	"severity": "error" }
		],
		"function-linear-gradient-no-nonstandard-direction": [ true,
			// Disallow direction values in linear-gradient() calls that are not valid according to the standard syntax.
			// https://stylelint.io/user-guide/rules/function-linear-gradient-no-nonstandard-direction/
			// https://developer.mozilla.org/en-US/docs/Web/CSS/linear-gradient#Syntax
						{	"severity": "error",
							"message": "Linear Gradient Function with Non-standard Direction" }
		],
		"media-feature-name-no-unknown": [ true,
			// Validates :  Features Used in @media rules (i.e. min-width, screen, etc.)
						{	"severity": "error",
							"ignoreMediaFeatureNames": [] // Add Valid, but Non-Standard Media Features to this Array
			}
		],
		"no-unknown-animations": [ null,
			// Validates :  Only 'known,' named keyframe animations are allowed.
			// Considers the identifiers of @keyframes rules defined within the same source to be known.
						{	"severity": "error" }
		],
		"property-no-unknown": [ true,
						{	"severity": "error" }
		],
		"selector-pseudo-class-no-unknown": [ true,
			// Validates : Pseudo-class selectors defined in the CSS Specifications (i.e. :hover, :focus, :not(p), etc.)
						{	"severity": "error",
							"ignorePseudoClasses": [] // Add Valid, but Non-Standard Pseudo-Classes to this Array
			}
		],
		"selector-pseudo-element-no-unknown": [ true,
			// Validates :  Pseudo-element selectors defined in the CSS Specifications (i.e. ::before, ::selection, etc.)
						{	"severity": "error",
							"ignorePseudoElements": [] // Add Valid, but Non-Standard Pseudo-Elements to this Array
			}
		],
		"selector-type-no-unknown": [ true,
			// Validates :  HTML, SVG, MathML Elements/Tags (i.e. a, li, div, etc.)
						{	"severity": "error",
							"ignore": [], // Options: "custom-elements", "default-namespace"
							"ignoreNamespaces": [], // Add Valid, but Non-Standard Element Type Namespaces to this Array
							"ignoreTypes": [] // Add Valid, but Non-Standard Element Types to this Array
			}
		],
		"string-no-newline": [ true,
						{	"severity": "error",
							"message": "Line-breaks in Strings Must be Escaped" }
		],
		"unit-no-unknown": [ true,
						{	"severity": "error" }
		],



		//
		//
		//  Clean-up Rules
		//  Deal with Duplicates and Verify Defaults
		// ====================================================================
		// ====================================================================

		"block-no-empty": [ true,
			{	"severity": "warning" }
		],
		"declaration-block-no-duplicate-properties": [ true,
			// Allows/Ignores Fallbacks: Duplicate properties with different values that are declared consecutively.
			// i.e. font-size: 12px; followed by font-size: 1.2rem;
						{	"severity": "warning",
							"ignore": "consecutive-duplicates" }
		],
		"declaration-block-no-shorthand-property-overrides": [ true,
			// Intended override longhand properties should come after the base shorthand properties
						{	"severity": "warning" }
		],
		"font-family-no-duplicate-names": [ true,
						{	"severity": "error" }
		],
		"font-family-no-missing-generic-family-keyword": [ null,
						{	"severity": "error" }
		],
		"no-duplicate-at-import-rules": [ true,
						{	"severity": "warning" }
		],
		"no-duplicate-selectors": [ true,
						{	"severity": "warning" }
		],



		//
		//
		//  Value Limitations
		// ====================================================================
		// ====================================================================

		"color-named": [ "never",
						{	"severity": "error",
							"message": "Name Values for Colors are Not Allowed !!!" }
		],
		"declaration-no-important": [ null,
			// Disallow !important within declarations.
						{	"severity": "error",
							"message": "Use of !important is Not Allowed !!!"
			}
		],
		"function-url-no-scheme-relative": [ true,
			// Disallows :  Disallow scheme-relative URLs ("//www.google.com/file.jpg").
			// Does Not Affect Rooted ("/somePathFromRoot/file.jpg") or Protocol Inclusive URLs ("http://www.google.com/file.jpg")
						{	"severity": "error" }
		],
		"keyframe-declaration-no-important": [ true,
						{	"severity": "warning",
							"message": "Avoid Using '!Important' in Keyframes, Some Browsers will Ignore It" }
		],
		"number-leading-zero": [ null, // "always" | "never"
			// Enforces :  Preference for requiring or disallowing a leading zero for fractional numbers less than 1.
						{	"severity": "error" }
		],
		"number-max-precision": [ 5,
			// Validates :  A limitation on the number of decimal places allowed in numbers.
			// Example :  2, would consider the 3.245634px a violation and 3.24px acceptible.
						{	"severity": "error",
							"ignoreUnits" : [] // Ignore the precision of numbers for values with the specified units (i.e. ["%"], etc.).
			}
		],
		"number-no-trailing-zeros": [ null, // true
			// Enforces :  Preference for disallowing trailing zeros in numbers.
			// Example :  '1.0px' and '1.01000px' would be disallowed.
						{	"severity": "error" }
		],
		"time-min-milliseconds": [ 100,
			// Validates :  Milliseconds for time values, must exceed the declared number.
			// Example :  100, would consider the 80ms a violation and 150ms acceptible.
						{	"severity": "error" }
		],



		//
		//
		//  Selector Length Management
		//  Limits on Selector Length and Specificity Definition
		// ====================================================================
		// ====================================================================

		"max-nesting-depth": [ null,
			// Validates :  Selector and rule nesting depths do not excede the defined number
						{	"severity": "error",
							"ignore": [],// Option: "blockless-at-rules"
							"ignoreAtRules": [] // Specified at-rules will not be included for depth calc
			}
		],
		"selector-max-attribute": [ null,
			// Limits the number of attribute selectors in a selector.
						{	"severity": "error" }
		],
		"selector-max-class": [ null,
			// Limits the number of class selectors in a selector.
			// Example :  2 will prevent any selector definitions with more than 2 classes
			// (i.e. '.foo.bar.baz' & '.foo .bar .baz' would both be invalid)
						{	"severity": "error" }
		],
		"selector-max-combinators": [ null,
			// Limits the number of combinators in a selector.
			// Example :  2 will prevent any selector definitions with more than 2 combinators
			// (i.e. 'a > b + c ~ d' & 'a b ~ c + d' would both be invalid)
						{	"severity": "error" }
		],
		"selector-max-compound-selectors": [ null,
			// Limits the number of compound selectors in a selector.
			// Example :  2 will prevent any selector definitions with more than 2 selectors elements
			// (i.e. '.foo .bar .baz' & 'div .bar[data-val] > a.baz' would both be invalid)
						{	"severity": "error" }
		],
		"selector-max-id": [ null,
			// Limits the number of IDs in a selector
			// Example :  2 will prevent any selector definitions with more than 2 IDs
			// (i.e. '#foo #bar #baz' & '#foo .bar #baz #grem' would both be invalid)
						{	"severity": "error" }
		],
		"selector-max-pseudo-class": [ null,
			// Limits the number of pseudo-classess in a selector
			// Example :  1 will prevent any selector definitions with more than 1 pseudo-class
			// (i.e. 'a:first-child:focus' & 'a:first-child:hover' would both be invalid)
						{	"severity": "error" }
		],
		"selector-max-specificity": [ null,
			// Limits the specificity of selectors.
			// Format :  String ('id,class,type'))
			// Example :  "0,2,0"
						{	"severity": "error" }
		],
		"selector-max-type": [ null,
			// Limits the number of types in a selector.
			// Example :  1 will prevent any selector definitions with more than 1 type
			// (i.e. 'div a' & 'div #menu a:hover' would both be invalid)
						{	"severity": "error" }
		],
		"selector-max-universal": [ null,
			// Limits the number of universal selectors (wildcards) in a selector.
			// Example :  1 will prevent any selector definitions with more than 1 universal selector
			// (i.e. '* .links *' & '* *' would both be invalid)
						{	"severity": "error" }
		],
		"selector-nested-pattern": [ null,
			// Specify a pattern for the selectors of rules nested within rules to be enforced
			// Example :  Given the RegEx string, '^&:(?:hover|focus)$', a nested selector like 'a { &:hover {} }
			// would be allowed, while 'a { .bar:hover {} }' would be a violation.
						{	"severity": "error" }
		],
		"selector-no-qualifying-type": [ null,
			// Validates :  Qualifying a selector by a type is Not Allowed
			// A type selector is "qualifying" when it is compounded with (chained to) another selector
			// (e.g. a.foo, a#foo)
						{	"severity": "error",
							"ignore": [] // Options: ["attribute", "class", "id"]
			}
		],



		//
		//
		//  Vendor Prefixing Rules
		// ====================================================================
		// ====================================================================

		"at-rule-no-vendor-prefix": [ null,
			// Validates :  Vendor Prefixes for at-rules are Not Allowed
			// Example: Turned on, '@-webkit-keyframes' would be a Violation as well as '@-ms-viewport'
						{	"severity": "error" }
		],
		"media-feature-name-no-vendor-prefix": [ null,
			// Validates :  Vendor Prefixes for Media Feature Names are Not Allowed
			// Example: Turned on, '@media (-webkit-min-device-pixel-ratio: 1) {}' would be a Violation
			// NOTE: Right now this rule simply checks for prefixed resolutions.
						{	"severity": "error" }
		],
		"selector-no-vendor-prefix": [ null,
			// Validates :  Vendor prefixes for selectors
			// This rule does not blanketly condemn vendor prefixes. Instead, it uses Autoprefixer's
			// up-to-date data (from caniuse.com) to know whether a vendor prefix should cause a violation or not.
						{	"severity": "error" }
		],



		//
		//
		//  Blacklists / Whitelists
		//  CSS Properties, etc. that should not be used
		// ====================================================================
		// ====================================================================

		"at-rule-blacklist": [ [],
			// Validates :  Against a specified blacklist of disallowed at-rules.
			// NOTE: This rule ignores @import in Less.
			// Example :  ["keyframes"] would disallow any @keyframes definitions, including vendor
			// prefixed @-moz-keyframes
						{	"severity": "error" }
		],
		"comment-word-blacklist": [ [],
			// Validates :  Against a specified blacklist of words that cannot be included in comment blocks.
			// Example :  ["TODO"] would disallow any comment like '/* TODO: something */'
						{	"severity": "error" }
		],
		"declaration-property-unit-blacklist": [ {},
			// Validates :  Against a specified blacklist of disallowed property and unit pairs
			// Example :  {"font-size": ["em", "px"]} would disallow use of 'em' and 'px' units for font-size properties,
						{	"severity": "error" }
		],
		"declaration-property-value-blacklist": [ {},
			// Validates :  Against a specified blacklist of disallowed property and value pairs
			// Example :  {"position": ["fixed"]} would disallow use of 'fixed' as a value for the position property,
						{	"severity": "error" }
		],
		"function-blacklist": [ [],
			// Validates :  Against a specified blacklist of disallowed functions (i.e. calc, scale, rgba, etc.).
						{	"severity": "error" }
		],
		"function-url-scheme-blacklist": [ [],
			// Validates :  Against a specified blacklist of disallowed URL schemes (i.e. ["ftp", "/^http/"], etc.).
						{	"severity": "error" }
		],
		"media-feature-name-blacklist": [ [],
			// Validates :  Against a specified blacklist of disallowed media feature names
			// Example :  ["max-width"] would disallow a media selector like, '@media (max-width: 50em) {}'
			// NOTE: Caveat: Media feature names within a range context are currently ignored.
						{	"severity": "error" }
		],
		"media-feature-name-value-whitelist": [ {},
			// Validates :  Against a specified whitelist of allowed media feature name and value pairs.
			// If a media feature name is found in this object, only its whitelisted values are allowed.
			// If a media feature name is not included in this object, anything goes for its value.
			// Example :  { "min-width": ["768px", "1024px"] } would disallow any @media (min-width) that
			// had a value other than 768px or 1024px.
						{	"severity": "error" }
		],
		"property-blacklist": [ [""],
			// Validates :  Against a specified blacklist of disallowed CSS properties (i.e. "px", "animation", "text-rendering", etc.).
						{	"severity": "error" }
		],
		"selector-attribute-operator-blacklist": [ [],
			// Validates :  Against a specified blacklist of disallowed attribute operators
			// Example :  ["*="] would disallow [class*="test"],
						{	"severity": "error" }
		],
		"selector-combinator-blacklist": [ [],
			// Validates :  Against a specified blacklist of disallowed selector combinators
			// Example :  [">", " "] would disallow selectors like 'a > b {}' & 'a b {}',
						{	"severity": "error" }
		],
		"selector-pseudo-class-blacklist": [ [],
			// Validates :  Against a specified blacklist of disallowed pseudo-class selectors
			// Example :  ["hover", "/^nth-/"] would disallow selectors like 'a:hover' & 'a:nth-child'
			// NOTE: This rule ignores selectors that use variable interpolation e.g. :#{$variable} {}
						{	"severity": "error" }
		],
		"selector-pseudo-element-blacklist": [ [],
			// Validates :  Against a specified blacklist of disallowed pseudo-element selectors
			// Example :  ["before", "after"] would disallow selectors like 'a::before' & '.myMenu::after',
						{	"severity": "error" }
		],
		"unit-blacklist": [ [""],
			// Validates :  Against a specified blacklist of disallowed value units (i.e. "px", "em", "deg", "%", etc.).
						{	"severity": "error" }
		],



		//
		//
		//  Pattern Definition Enforcement
		// ====================================================================
		// ====================================================================

		"custom-media-pattern": [ "",
			// Specify a pattern for custom media query names to be enforced
			// Example: "foo-.+" would allow '@custom-media --foo-bar (min-width: 30em);'.
			// and Disallow '@custom-media --bar (min-width: 30em);'
						{	"severity": "error" }
		],
		"custom-property-pattern": [ "",
			// Specify a pattern for custom properties names to be enforced
			// Example: "foo-.+" would allow "--foo-bar: 0;", and disallow "--boo-bar: 0;".
						{	"severity": "error" }
		],
		"keyframes-name-pattern": [ "",
			// Specify a pattern for keyframe names to be enforced
			// Example: "foo-.+" would allow "@keyframes foo-bar", and disallow "@keyframes bar".
						{	"severity": "error" }
		],
		"selector-class-pattern": [ "",
			// Specify a pattern for css class names/selectors to be enforced
			// Example: "foo-[a-z]+" would allow '.foo-bar' and disallow '.menu' and '.foo-BAR'.
						{	"severity": "error" }
		],
		"selector-id-pattern": [ "",
			// Specify a pattern for ID names/selectors to be enforced
			// Example: "foo-[a-z]+" would allow '#foo-bar' and disallow '#main-menu' and '#foo-BAR'.
						{	"severity": "error" }
		],



		//
		//
		//  Text-Case Preference Rules
		// ====================================================================
		// ====================================================================

		"at-rule-name-case": [ null, // "lower" | "upper"
			// Enforces :  Preference for lowercase or uppercase for at-rules names.
			// NOTES :  This rule ignores @import in Less. Only lowercase at-rule names are valid in SCSS.
			// Example :  Set to lower, '@charset 'UTF-8';' would be valid, '@Charset 'UTF-8';' would Not
						{	"severity": "error" }
		],
		"color-hex-case": [ null, // "lower" | "upper"
			// Enforces :  Preference for Lowercase or Uppercase Alpha-Characters in Hex-color Values
						{	"severity": "error" }
		],
		"function-name-case": [ null, // "lower" | "upper"
			// Enforces :  Preference for lowercase or uppercase function names.
			// NOTES :  Camel case function names, e.g. translateX, are accounted for when the lower option is used..
			// Example :  Set to lower, 'width: calc(5% - 10em);' would be valid, 'width: Calc(5% - 10em);' would Not
						{	"severity": "error",
							"ignoreFunctions" : [] // Array of function names to ignore case usage on
			}
		],
		"media-feature-name-case": [ null, // "lower" | "upper"
			// Enforces :  Preference for lowercase or uppercase for media feature names.
			// Example :  Set to lower, '@media (min-width: 700px) {}' would be valid, '@media (MIN-WIDTH: 700px) {}' would Not
						{	"severity": "error" }
		],
		"property-case": [ null, // "lower" | "upper"
			// Enforces :  Preference for lowercase or uppercase for properties.
			// Example :  Set to lower, 'display: block;' would be valid, 'Display: block;' would Not
						{	"severity": "error" }
		],
		"selector-pseudo-class-case": [ null, // "lower" | "upper"
			// Enforces :  Preference for lowercase or uppercase for pseudo-class selectors.
			// Example :  Set to lower, 'div:hover {}' would be valid, 'div:Hover {}' would Not
						{	"severity": "error" }
		],
		"selector-pseudo-element-case": [ null, // "lower" | "upper"
			// Enforces :  Preference for lowercase or uppercase for pseudo-element selectors.
			// Example :  Set to lower, 'a::before {}' would be valid, 'a::Before {}' would Not
						{	"severity": "error" }
		],
		"selector-type-case": [ null, // "lower" | "upper"
			// Enforces :  Preference for lowercase or uppercase for type selectors.
			// Example :  Set to lower, 'a {}' would be valid, 'A {}' would Not
						{	"severity": "error" }
		],
		"unit-case": [ null, // "lower" | "upper"
			// Enforces :  Preference for lowercase or uppercase units.
			// Example :  Set to lower, ' width: 10px;' would be valid, ' width: 10PX;' would Not
						{	"severity": "error" }
		],
		"value-keyword-case": [ null, // "lower" | "upper"
			// Enforces :  Preference for lowercase or uppercase for value keywords.
			// Example :  Set to lower, 'display: block;' would be valid, 'display: Block;' would Not
						{	"severity": "error",
							"ignoreKeywords": [], // Ignore the casing of listed keywords values,
							"ignoreProperties": [] // Ignore the casing of values for listed properties (e.g. 'display')
			}
		],



		//
		//
		//  File Format Enforcement
		// ====================================================================
		// ====================================================================

		"linebreaks": [ null, // "unix" | "windows"
			// Enforces :  Preference for indentation.
						{	"severity": "error" }
		],
		"no-eol-whitespace": [ null, // true
			// Enforces :  Preference to disallow end-of-line whitespace.
						{	"severity": "error",
							"ignore": [] // Options: "empty-lines"
			}
		],
		"no-missing-end-of-source-newline": [ null, // true
			// Enforces :  Preference to disallow missing end-of-source newlines.
			// NOTES :  Completely empty files are not considered violations.
						{	"severity": "error" }
		],



		//
		//
		//  Stylistic File Format Preference Rules
		// ====================================================================
		// ====================================================================

		"function-calc-no-unspaced-operator": [ true,
			{	"severity": "warning",
				"message": "Operators (+, -, etc) in Calc functions should have spaces on both sides." }
		],
		"comment-no-empty": [ true,
					{	"severity": "warning" }
		],
		"no-extra-semicolons": [ true,
					{	"severity": "warning" }
		],
		"declaration-block-single-line-max-declarations": [ null,
			// Validates :  Against a limitation on the number of declarations within a single line.
			// Example :  1 would require no-more than one declaration per line.
						{	"severity": "error" }
		],
		"selector-max-empty-lines": [ null,
			// Limit the number of adjacent empty lines within selectors.
						{	"severity": "error" }
		],
		"color-hex-length": [ null, // "short" | "long"
			// Enforces :  Preference for Short or Long Notation for Hex Color Values.
						{	"severity": "error" }
		],
		"font-family-name-quotes": [ null, // "always-where-required" | "always-where-recommended" | "always-unless-keyword"
			// Enforces :  Preference for whether or not quotation marks should be used around font family names.
						{	"severity": "error" }
		],
		"font-weight-notation": [ null, // "numeric" | "named-where-possible"
			// Enforces :  Preference for numeric or named values for font-weight.
			// Also, when named values are expected, validates name values.
						{	"severity": "error",
							"ignore" : "" // Option: "relative" - Ignore the relative keyword names (i.e. bolder and lighter).
			}
		],
		"function-comma-newline-after": [ null, // "always" | "always-multi-line" | "never-multi-line"
			// Enforces :  Preference for requiring a newline or disallowing whitespace after the commas of functions.
						{	"severity": "error" }
		],
		"function-comma-newline-before": [ null, // "always" | "always-multi-line" | "never-multi-line"
			// Enforces :  Preference for requiring a newline or disallowing whitespace before the commas of functions.
						{	"severity": "error" }
		],
		"function-comma-space-after": [ null, // "always" | "never" | "always-single-line" | "never-single-line"
			// Enforces :  Preference for requiring a single space or disallowing whitespace after the commas of functions.
						{	"severity": "error" }
		],
		"function-comma-space-before": [ null, // "always" | "never" | "always-single-line" | "never-single-line"
			// Enforces :  Preference for requiring a single space or disallowing whitespace before the commas of functions.
						{	"severity": "error" }
		],
		"function-max-empty-lines": [ null, // int
			// Enforces :  Preference for limitting the number of adjacent empty lines within functions.
						{	"severity": "error" }
		],
		"function-parentheses-newline-inside": [ null, // "always" | "always-multi-line" | "never-multi-line"
			// Enforces :  Preference for requiring a newline or disallowing whitespace on the inside of the parentheses of functions.
						{	"severity": "error" }
		],
		"function-parentheses-space-inside": [ null, // "always" | "never" | "always-single-line" | "never-single-line"
			// Enforces :  Preference for requiring a single space or disallowing whitespace on the inside of the parentheses of functions.
						{	"severity": "error" }
		],
		"function-url-quotes": [ null, // "always" | "never"
			// Enforces :  Preference for requiring or disallowing quotes for URLs.
						{	"severity": "error",
							"except": "" // Option: "empty" - Reverse the primary setting if the function has no arguments.
			}
		],
		"function-whitespace-after": [ null, // "always" | "never"
			// Enforces :  Preference for requiring or disallowing whitespace after functions.
						{	"severity": "error" }
		],
		"string-quotes": [ null, // "single" | "double"
			// Enforces :  Preference for single or double quotes around strings.
						{	"severity": "error",
							"avoidEscape": true // Options: true|false, defaults to true
												// Allows strings to use single-quotes or double-quotes so long as the string
												// contains a quote that would have to be escaped otherwise.
			}
		],
		"length-zero-no-unit": [ null, // true
			// Enforces :  Preference to disallow declaration of units for lengths with a value of zero.
			// Example :  'top: 0px;' would be a violation. It should be just 'top: 0;'
						{	"severity": "error",
							"ignore": [] // Options: "custom-properties" - Ignore units for zero length in custom properties.
												// (e.g. '--myProp: 0px;' would be allowed)
			}
		],
		"value-list-comma-newline-after": [ null, // "always" | "always-multi-line" | "never-multi-line"
			// Enforces :  Preference for requiring a new line or disallowing whitespace after the commas of value lists.
						{	"severity": "error" }
		],
		"value-list-comma-newline-before": [ null, // "always" | "always-multi-line" | "never-multi-line"
			// Enforces :  Preference for requiring a new line or disallowing whitespace before the commas of value lists.
						{	"severity": "error" }
		],
		"value-list-comma-space-after": [ null, // "always" | "never" | "always-single-line" | "never-single-line"
			// Enforces :  Preference for requiring a single space or disallowing whitespace after the commas of value lists.
						{	"severity": "error" }
		],
		"value-list-comma-space-before": [ null, // "always" | "never" | "always-single-line" | "never-single-line"
			// Enforces :  Preference for requiring a single space or disallowing whitespace before the commas of value lists.
						{	"severity": "error" }
		],
		"value-list-max-empty-lines": [ null, // int
			// Enforces :  Preference to limit the number of adjacent empty lines within value lists.
						{	"severity": "error" }
		],
		"custom-property-empty-line-before": [ null, // "always" | "never"
			// Enforces :  Preference for requiring or disallowing an empty line before custom properties.
						{	"severity": "error" }
		],
		"declaration-bang-space-after": [ null, // "always" | "never"
			// Enforces :  Preference requiring a single space or disallowing whitespace after the bang of declarations.
			// Example :  Set to always, 'display: block ! important;' would be valid, 'display: block !important;' would Not
						{	"severity": "error" }
		],
		"declaration-bang-space-before": [ null, // "always" | "never"
			// Enforces :  Preference requiring a single space or disallowing whitespace before the bang of declarations.
			// Example :  Set to always, 'display: block !important;' would be valid, 'display: block!important;' would Not
						{	"severity": "error" }
		],
		"declaration-colon-newline-after": [ null, // "always" | "always-multi-line"
			// Enforces :  Preference for requiring a new line or disallowing whitespace after the colon of declarations.
						{	"severity": "error" }
		],
		"declaration-colon-space-after": [ null, // "always" | "never" | "always-single-line"
			// Enforces :  Preference for requiring a single space or disallowing whitespace after the colon of declarations.
						{	"severity": "error" }
		],
		"declaration-colon-space-before": [ null, // "always" | "never"
			// Enforces :  Preference for requiring a single space or disallowing whitespace before the colon of declarations.
						{	"severity": "error" }
		],
		"declaration-empty-line-before": [ null, // "always" | "never"
			// Enforces :  Preference for requiring or disallowing an empty line before declarations.
						{	"severity": "error",
							"except": [], // Options: "after-comment", "after-declaration", "first-nested"
							"ignore": [] // Options: "after-comment", "after-declaration", "first-nested", "inside-single-line-block"
			}
		],
		"declaration-block-semicolon-newline-after": [ null, // "always" | "always-multi-line" | "never-multi-line"
			// Enforces :  Preference for requiring a new line or disallowing whitespace after the semicolons of declaration blocks.
						{	"severity": "error" }
		],
		"declaration-block-semicolon-newline-before": [ null, // "always" | "always-multi-line" | "never-multi-line"
			// Enforces :  Preference for requiring a new line or disallowing whitespace before the semicolons of declaration blocks.
						{	"severity": "error" }
		],
		"declaration-block-semicolon-space-after": [ null, // "always" | "never" | "always-single-line" | "never-single-line"
			// Enforces :  Preference for requiring a single space or disallowing whitespace after the semicolons of declaration blocks.
						{	"severity": "error" }
		],
		"declaration-block-semicolon-space-before": [ null, // "always" | "never" | "always-single-line" | "never-single-line"
			// Enforces :  Preference for requiring a single space or disallowing whitespace before the semicolons of declaration blocks.
						{	"severity": "error" }
		],
		"declaration-block-trailing-semicolon": [ null, // "always" | "never"
			// Enforces :  Preference for requiring or disallowing a trailing semicolon within declaration blocks.
						{	"severity": "error" }
		],
		"block-closing-brace-empty-line-before": [ null, // "always-multi-line" | "never"
			// Enforces :  Preference for requiring or disallowing an empty line before the closing brace of blocks.
						{	"severity": "error",
							"except": [] // Options: "after-closing-brace"
			}
		],
		"block-closing-brace-newline-after": [ null, // "always" | "always-single-line" | "never-single-line" | "always-multi-line"
														// | "never-multi-line"
			// Enforces :  Preference for requiring a new line or disallowing whitespace after the closing brace of blocks.
						{	"severity": "error",
							"ignoreAtRules": [] // List of at-rules to ignore.
			}
		],
		"block-closing-brace-newline-before": [ null, // "always" | "always-multi-line" | "never-multi-line"
			// Enforces :  Preference for requiring a new line or disallowing whitespace before the closing brace of blocks.
						{	"severity": "error" }
		],
		"block-closing-brace-space-after": [ null, // "always" | "never" | "always-single-line" | "never-single-line"
													// | "always-multi-line" | "never-multi-line"
			// Enforces :  Preference for requiring a single space or disallowing whitespace after the closing brace of blocks.
						{	"severity": "error" }
		],
		"block-closing-brace-space-before": [ null, // "always" | "never" | "always-single-line" | "never-single-line"
													// | "always-multi-line" | "never-multi-line"
			// Enforces :  Preference for requiring a single space or disallowing whitespace before the closing brace of blocks.
						{	"severity": "error" }
		],
		"block-opening-brace-newline-after": [ null, // "always" | "always-multi-line" | "never-multi-line"
			// Enforces :  Preference for requiring a new line after the opening brace of blocks.
						{	"severity": "error" }
		],
		"block-opening-brace-newline-before": [ null, // "always" | "always-single-line" | "never-single-line"
														// | "always-multi-line" | "never-multi-line"
			// Enforces :  Preference for requiring a new line  or disallowing whitespace before the opening brace of blocks.
						{	"severity": "error" }
		],
		"block-opening-brace-space-after": [ null, // "always" | "always-single-line" | "never-single-line"
														// | "always-multi-line" | "never-multi-line"
			// Enforces :  Preference for requiring a single space or disallowing whitespace after the opening brace of blocks.
						{	"severity": "error" }
		],
		"block-opening-brace-space-before": [ null, // "always" | "never" | "always-single-line" | "never-single-line"
													// | "always-multi-line" | "never-multi-line"
			// Enforces :  Preference for requiring a single space or disallowing whitespace before the opening brace of blocks.
						{	"severity": "error",
							"ignoreAtRules": [] // List of at-rules to ignore.
			}
		],
		"selector-attribute-brackets-space-inside": [ null, // "always" | "never"
			// Enforces :  Preference for requiring a single space or disallowing whitespace on the inside of the brackets
			// within attribute selectors.
						{	"severity": "error" }
		],
		"selector-attribute-operator-space-after": [ null, // "always" | "never"
			// Enforces :  Preference for requiring a single space or disallowing whitespace after operators
			// within attribute selectors.
						{	"severity": "error" }
		],
		"selector-attribute-operator-space-before": [ null, // "always" | "never"
			// Enforces :  Preference for requiring a single space or disallowing whitespace before operators
			// within attribute selectors.
						{	"severity": "error" }
		],
		"selector-attribute-quotes": [ null, // "always" | "never"
			// Enforces :  Preference for whether to require or disallow quotation marks for attribute values.
						{	"severity": "error" }
		],
		"selector-combinator-space-after": [ null, // "always" | "never"
			// Enforces :  Preference for requiring a single space or disallowing whitespace after the combinators of selectors.
						{	"severity": "error" }
		],
		"selector-combinator-space-before": [ null, // "always" | "never"
			// Enforces :  Preference for requiring a single space or disallowing whitespace before the combinators of selectors.
						{	"severity": "error" }
		],
		"selector-descendant-combinator-no-non-space": [ null, // true
			// Enforces :  Preference to disallow non-single-space characters for descendant combinators of selectors.
						{	"severity": "error" }
		],
		"selector-pseudo-class-parentheses-space-inside": [ null, // "always" | "never"
			// Enforces :  Preference for requiring a single space or disallowing whitespace on the inside of
			// the parentheses within pseudo-class selectors.
						{	"severity": "error" }
		],
		"selector-pseudo-element-colon-notation": [ null, // "single" | "double"
			// Enforces :  Preference for single or double colon notation for applicable pseudo-elements.
			// Example :  Set to double, 'a::before {}' would be valid, 'a:before {}' would Not
						{	"severity": "error" }
		],
		"selector-list-comma-newline-after": [ null, // "always" | "always-multi-line" | "never-multi-line"
			// Enforces :  Preference for requiring a new line or disallowing whitespace after the commas of selector lists.
						{	"severity": "error" }
		],
		"selector-list-comma-newline-before": [ null, // "always" | "always-multi-line" | "never-multi-line"
			// Enforces :  Preference for requiring a new line or disallowing whitespace before the commas of selector lists.
						{	"severity": "error" }
		],
		"selector-list-comma-space-after": [ null, // "always" | "never" | "always-single-line" | "never-single-line"
			// Enforces :  Preference for requiring a single space or disallowing whitespace after the commas of selector lists.
						{	"severity": "error" }
		],
		"selector-list-comma-space-before": [ null, // "always" | "never" | "always-single-line" | "never-single-line"
			// Enforces :  Preference for requiring a single space or disallowing whitespace before the commas of selector lists.
						{	"severity": "error" }
		],
		"rule-empty-line-before": [ null, // "always" | "never" | "always-multi-line" | "never-multi-line"
			// Enforces :  Preference for requiring or disallowing a empty line before rules.
						{	"severity": "error",
							"except": [], // Options: "after-rule", "after-single-line-comment", "inside-block-and-after-rule",
											// 			"inside-block", "first-nested"
							"ignore": [], // Options: "after-comment", "first-nested", "inside-block"
			}
		],
		"media-feature-colon-space-after": [ null, // "always" | "never"
			// Enforces :  Preference for requiring a single space or disallowing whitespace after the colon in media features.
						{	"severity": "error" }
		],
		"media-feature-colon-space-before": [ null, // "always" | "never"
			// Enforces :  Preference for requiring a single space or disallowing whitespace before the colon in media features.
						{	"severity": "error" }
		],
		"media-feature-parentheses-space-inside": [ null, // "always" | "never"
			// Enforces :  Preference for requiring a single space or disallowing whitespace on the inside of the parentheses
			// within media features.
						{	"severity": "error" }
		],
		"media-feature-range-operator-space-after": [ null, // "always" | "never"
			// Enforces :  Preference for requiring a single space or disallowing whitespace after the range operator in media features.
						{	"severity": "error" }
		],
		"media-feature-range-operator-space-before": [ null, // "always" | "never"
			// Enforces :  Preference for requiring a single space or disallowing whitespace before the range operator in media features.
						{	"severity": "error" }
		],
		"media-query-list-comma-newline-after": [ null, // "always" | "always-multi-line" | "never-multi-line"
			// Enforces :  Preference for requiring a new line or disallowing whitespace after the commas of media query lists.
						{	"severity": "error" }
		],
		"media-query-list-comma-newline-before": [ null, // "always" | "always-multi-line" | "never-multi-line"
			// Enforces :  Preference for requiring a new line or disallowing whitespace before the commas of media query lists.
						{	"severity": "error" }
		],
		"media-query-list-comma-space-after": [ null, // "always" | "never" | "always-single-line" | "never-single-line"
			// Enforces :  Preference for requiring a single space or disallowing whitespace after the commas of media query lists.
						{	"severity": "error" }
		],
		"media-query-list-comma-space-before": [ null, // "always" | "never" | "always-single-line" | "never-single-line"
			// Enforces :  Preference for requiring a single space or disallowing whitespace before the commas of media query lists.
						{	"severity": "error" }
		],
		"at-rule-empty-line-before": [ null, // "always" | "never"
			// Enforces :  Preference for requiring or disallowing a empty line before at-rules.
						{	"severity": "error",
							"except": [], // Options: "after-same-name", "inside-block", "blockless-after-same-name-blockless",
										//			"blockless-after-blockless", "first-nested"
							"ignore": [], // Options: "after-comment", "first-nested", "inside-block",
										//			"blockless-after-same-name-blockless", "blockless-after-blockless"
							"ignoreAtRules": [] //  List of specific At-Rules to ignore
			}
		],
		"at-rule-name-newline-after": [ null, // "always" | "always-multi-line"
			// Enforces :  Preference for requiring a new line after at-rule names.
			// NOTES :  This rule ignores @import in Less.
						{	"severity": "error" }
		],
		"at-rule-name-space-after": [ null, // "always" | "always-single-line"
			// Enforces :  Preference for requiring a single space after at-rule names.
						{	"severity": "error" }
		],
		"at-rule-semicolon-newline-after": [ null, // "always"
			// Enforces :  Preference for requiring a new line after the semicolon of at-rules.
			// NOTES :  This rule ignores @import in Less.
						{	"severity": "error" }
		],
		"at-rule-semicolon-space-before": [ null, // "always" | "never"
			// Enforces :  Preference for requiring a single space or disallowing whitespace before the semicolons at-rules.
						{	"severity": "error" }
		],
		"comment-empty-line-before": [ null, // "always" | "never"
			// Enforces :  Preference for requiring or disallowing a empty line before comments.
			// NOTES :  This rule ignores:	comments that are the very first node in the source
			//								shared-line comments
			//								single-line comments with // (when you're using a custom syntax that supports them)
			//								comments within selector and value lists
						{	"severity": "error",
							"except": [], // Options: "first-nested"
							"ignore": [] // Options: "after-comment", "stylelint-commands"
			}
		],
		"comment-whitespace-inside": [ null, // "always" | "never"
			// Enforces :  Preference for requiring or disallowing whitespace on the inside of comment markers.
			// NOTES :  Any number of asterisks are allowed at the beginning or end of the comment. So /** comment **/ is treated
			//			the same way as /* comment */.
			// Caveat: Comments within selector and value lists are currently ignored.
						{	"severity": "error" }
		],
		"indentation": [ null, // int | "tab", where int is the number of spaces
			// Enforces :  Preference for indentation.
						{	"severity": "error",
							"indentInsideParens": "", // Options: "twice" | "once-at-root-twice-in-block"
							"indentClosingBrace": false, // Options: true | false
							"except": [], // Options: "block", "param", "value"
							"ignore": [] // Options: "inside-parens", "param", "value"
			}
		],
		"max-empty-lines": [ null, // int
			// Enforces :  Preference for limitting the number of adjacent empty lines.
						{	"severity": "error",
							"ignore": [] // Options: "comments"
			}
		],
		"max-line-length": [ null, // int
			// Enforces :  Preference for limitting the number characters/length of a line.
			// NOTES :  Lines that exceed the maximum length but contain no whitespace (other than at the beginning of the line)
			// 			are ignored.
						{	"severity": "error",
							"ignore": [], // Options: "comments", "non-comments"
							"ignorePattern": "" // RegEex Pattern to ignore any line that matches,
												// regardless of whether it is comment or not.
			}
		],

	}
}
