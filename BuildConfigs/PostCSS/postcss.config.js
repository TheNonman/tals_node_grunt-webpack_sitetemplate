/* eslint-env node */
const { resolve } = require('path')

// fallback to default 'snes' theme if none is provided
const theme = process.env.THEME || 'snes'
const variablesFileID = ".variables";

module.exports = {
	plugins: {
		'postcss-import': {
			// Custom resolver that adds the theme extension to @import paths
			// Adjusted to only change @import paths to files that include '.variables'
			// see: https://github.com/postcss/postcss-import#resolve
			resolve: (id, basedir) => {
				if ( id.includes(variablesFileID) ) {
					return resolve(basedir, `${id}.${theme}.css`);
				}
				else {
					return resolve(basedir, `${id}.css`);
				}
			},
		},
		'postcss-custom-properties': {
			// Resolves all uses of CSS Variables to hard-coded values.
			preserve: false // Strip all CSS Variable defintions and Var resolusion functions (i.e. Supported Fallbacks)
		},

		'postcss-calc': {
			// Execute calc functions at build-time and replace them with their resolved value in output
			// Throw a warning when encountering a calc function that cannot be build-time resolved
			// (e.g. calc functions that include mixed value units, like % and px)
			warnWhenCannotResolve: true
		},
	},
}
