
//
//
// NodeJS Libs
// ================================================================================

const chalk = require('chalk');
const path = require('path');
const merge = require('webpack-merge');

//
//
// Webpack Plugins
// ================================================================================

const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const WebpackShellPlugin = require('webpack-shell-plugin');
// Custom
const WebpackFlowLogger = require('./CustomPlugins/WebpackFlowNotifier');


//
//
// Base Common Config
// ================================================================================

const wpCommonConfig = require('./webpack.common.config.js');

//
//
// Logging Hookup
// ================================================================================

const winston = require('winston');

let logger = null;
if (winston.loggers.has("webpack")) {
	logger = winston.loggers.get("webpack");
	logger.info('Webpack.Dev Config Attached to Webpack Build Winston Logger');
}
else {
	console.log(chalk.red('Webpack.Dev Config FAILED to Attach Webpack Build Winston Logger !!!'));
}



//
//
// Merge to Common Configuration
// ================================================================================

module.exports = merge(wpCommonConfig, {
	devtool: 'inline-source-map',
	module: {
		rules: [{
			test: /\.scss$/,
			use: [{
				loader: MiniCssExtractPlugin.loader
			}, {
				loader: "css-loader"
			}, {
				loader: "sass-loader",
				options: {
					includePaths: ["absolute/path/a", "absolute/path/b"]
				}
			}]
		}]
	},
	plugins: [
		new CleanWebpackPlugin(['_Staging'], { root: path.resolve(__dirname , '../../'), allowExternal: false }),
		new CopyWebpackPlugin([
			{ from: 'AppSource/SOURCE_HTML/index.html', to: './index.html' },
			{ from: 'AppSource/SOURCE_IMAGES/', to: './images/' },
			{ from: 'AppSource/SOURCE_FONTS/', to: './fonts/' }
		]),
		new WebpackFlowLogger({mainLogger: logger}),
		new WebpackShellPlugin({ verbose: true, onBuildEnd: ["node ./BuildConfigs/NodeScripts/ExternalSASS.js"] })
	]
})
