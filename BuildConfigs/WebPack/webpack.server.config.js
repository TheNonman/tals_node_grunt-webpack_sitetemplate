
//
//
// NodeJS Libs
// ================================================================================

const chalk = require('chalk');
const path = require('path');
const merge = require('webpack-merge');

//
//
// Webpack Plugins
// ================================================================================

const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const WebpackShellPlugin = require('webpack-shell-plugin');
// Custom
const WebpackFlowLogger = require('./CustomPlugins/WebpackFlowNotifier');


//
//
// Base Common Config
// ================================================================================

const wpCommonConfig = require('./webpack.common.config.js');

//
//
// Logging Hookup
// ================================================================================

const winston = require('winston');

let logger = null;
if (winston.loggers.has("webpack")) {
	logger = winston.loggers.get("webpack");
	console.log(chalk.blue('Webpack.Server Config Attached to Webpack Build Winston Logger'));
}
else {
	console.log(chalk.red('Webpack.Server Config FAILED to Attach Webpack Build Winston Logger !!!'));
}
logger.info('Webpack SERVER Config');



//
//
// Merge to Common Configuration
// ================================================================================

module.exports = merge(wpCommonConfig, {
	output: {
		publicPath: '/'
	},
	plugins: [
		new WebpackShellPlugin({ onBuildEnd: ["node ./BuildConfigs/NodeScripts/ExternalSASS.js"] }),
		new CopyWebpackPlugin([
			{ from: 'AppSource/SOURCE_HTML/index.html', to: './index.html' },
			{ from: 'AppSource/SOURCE_IMAGES/', to: './images/' },
			{ from: 'AppSource/SOURCE_FONTS/', to: './fonts/' },
			{ from: '_Staging/css', to: './css/' }
		]),
		new WebpackFlowLogger({mainLogger: logger}),
	]
})
