"use strict";

class WebpackFlowNotifier {
	constructor (options) {
		if (options && options.mainLogger) {
			this.logger = options.mainLogger;

			this.logger.info('WebpackFlowNotifier: Using Recieved Logger.');
		}
		else {
			const winston = require('winston');
			this.logger = winston.createLogger({
				transports: [
					new winston.transports.File({
						filename: './__BuildLogs/WebpackFlowNotifier.log'
					})
				]
			});
		}
	}

	apply(compiler) {
		compiler.hooks.compile.tap(
			'WebpackFlowNotifier', (compilationParams) => {
				//const time = ((stats.endTime - stats.startTime) / 1000).toFixed(2);

				this.logger.info(`Webpack Start Compilation`);
		});
		compiler.hooks.done.tap(
			'WebpackFlowNotifier', (stats) => {
				const time = ((stats.endTime - stats.startTime) / 1000).toFixed(2);

				this.logger.info(`Webpack Compilation is done!\nCompilation completed with ${stats.compilation.errors.length} errors in ${time}s`);
		});
		compiler.hooks.emit.tapAsync({
				name: 'WebpackFlowNotifier',
				context: true
			},
			(context, compiler, callback) => {

				this.logger.info(`Webpack Compiler Starting to Emit Assets`);
				this.logger.info(`    Will Emit: `);
				for (var filename in compiler.assets) {
					this.logger.info(`        ` + filename);
				}
				/*
				// Create a header string for the generated file:
				var filelist = 'In this build:\n\n';

				// Loop through all compiled assets,
				// adding a new line item for each filename.
				for (var filename in compilation.assets) {
					filelist += '- ' + filename + '\n';
				}

				// Insert this list into the webpack build as a new file asset:
				compiler.assets['filelist.md'] = {
					source: function () {
						return filelist;
					},
					size: function () {
						return filelist.length;
					}
				};

				this.logger.info('filelist.md generated.');
				*/
				callback();
			}
		);
		compiler.hooks.afterEmit.tapAsync({
				name: 'WebpackFlowNotifier',
				context: true
			},
			(context, compiler, callback) => {

				this.logger.info(`Webpack Compiler Assets Emitted to Output Directory`);
				callback();
			}
		);

		compiler.hooks.run.tapPromise(
			'WebpackFlowNotifier',
			(source, target, routesList) => {
				return new Promise((resolve, reject) => {

					this.logger.info(`Webpack Compiler Run (Start Compiliation) `);

					// Promise Fail
					//reject(err);
					// Promise Success
					resolve();
				});
			}
		);
		compiler.hooks.watchRun.tapPromise(
			'WebpackFlowNotifier',
			(source, target, routesList) => {
				return new Promise((resolve, reject) => {

					this.logger.info(`Webpack Compiler Watch-Based Run (Start Additional Compiliation) `);

					// Promise Fail
					//reject(err);
					// Promise Success
					resolve();
				});
			}
		);
	}
}

module.exports = WebpackFlowNotifier;
