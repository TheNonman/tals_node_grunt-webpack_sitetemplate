
const chalk = require('chalk');
const fs = require('fs');
const path = require('path');
const sass = require('node-sass');

const projectRoot = path.resolve(__dirname, '../../');
const outputPath = path.resolve(projectRoot, '_Staging/css');
const outputFile = 'main.css';


sass.render({
	file: path.resolve(projectRoot, 'AppSource/SOURCE_STYLES/app_main.scss'),
	outFile: outputPath + "/" + outputFile
}, function (error, result) { // node-style callback from v3.0.0 onwards
	if (!error) {
		// No errors during the compilation, write this result on the disk
		fs.writeFile(outputPath + "/" + outputFile, result.css, function (err) {
			if (!err) {
				//file written on disk
			}
		});
		console.log(chalk.green('ExternalSASS : SCSS Compilation Complete'));
	}
	else {
		console.log(chalk.red('ExternalSASS : SCSS Compilation Failed !!!'));
	}
});
