//
// IMPORTANT WINSTON LOGGER NOTES
//
// winston.loggers.add : When using this method the string id (1st argument) must be lower-case.
//
// winston.loggers.get : When using this method, if the string id passed does not exist, a new logger with
// the passed id will be created. To properly check for an existing logger, the winston.loggers.has method must
// be used instead.
//

const winston = require('winston');
const { format } = winston;
const { combine, timestamp, label, json, prettyPrint } = format;

const simpleConsole = format.combine(
	format.colorize(),
	format.align(),
	format.simple(),
	format.printf(info => `${info.level}: ${info.message}`)
  );

// Setup and configure for "webpack" logger
winston.loggers.add('webpack', {
	format: combine(
				label( { label: 'Webpack' } ),
				timestamp(),
				format.splat(),
				json()
	),
	level: 'silly',
	transports: [
		new winston.transports.File({ filename: './__BuildLogs/Webpack.log' }),
		new winston.transports.Console({ format: simpleConsole} )
	]
});

//
// Setup and configure for "webpack" logger
//
winston.loggers.add('logger2', {
	format: combine(
		label({
			label: 'category two'
		}),
		json()
	),
	transports: [
		new winston.transports.File({ filename: './__BuildLogs/logger2.log' })
	]
});

logger = winston.loggers.get("webpack");
logger.info('Winston Logger Active');

module.exports = winston;
