const request = require('request');
var requestProm = require('request-promise');
const fetch = require('node-fetch');


/*
// Fetch a random joke
function fetchQuote() {
	return fetch("https://api.icndb.com/jokes/random")
	.then(function (resp) {
		return resp.json();
	})
	.then(function (data) {
		return data.value.joke;
	});
}

async function sayJoke() {
	try {
		let result = await fetchQuote();
		console.log("Joke:", result);
	} catch (err) {
		console.error(err);
	}
}


requestProm({
	"method": "GET",
	"uri": "https://api.github.com/",
	"json": true,
	"headers": {
		"User-Agent": "My little demo app"
	}
}).then((res) => {
	console.log("GitHub API Response: %o", res);
	console.log("Start requestWithRetrys()...");
	sayJoke();
});
*/

/*creat a promise to get a paper   */
let getPaper = new Promise((resolve, reject) => {
	//do something
	let paper = true;
	if (paper) setTimeout(() => resolve("I got a paper"), 3000);
	else reject(" Cant find any paper");
});

//getPaper.then(data => console.log(data)).catch(data => console.log(data));

/*  creat a promise to write a letter   */
let writeLetter = new Promise((resolve, reject) => {
	let letter = true;
	if (letter) setTimeout(() => resolve("I wrote the letter"), 2000);
	else reject("i forgot to write a letter");
});

/*  creat a promise to post a letter   */

let postLetter = function (suc, fail) {
	return new Promise((suc, fail) => {
	posted = true;
	if (posted) setTimeout(() => suc("i posted the letter"), 5000);
	else fail("Post office is closed");
	})
};

const recurProm = function() {
		Promise.resolve()
		.then(
			function() {
				console.group( "Recursion - external function." );
				return( 3 );
			}
		)
		.then(
			function( n ) {
				// With the recursive function factored out into its own stand-alone
				// function, it's a lot easier to see that we are creating a totally
				// TANGENTIAL BRANCH of the Promise chain.
				// --
				// A
				// |
				// B --> B'3 --> B'2 --> B'1 --> B'0
				// |
				// C
				var tangentialPromiseBranch = recurseToZero( n );
				return( tangentialPromiseBranch );
			}
		)
		.then(
			function() {
				console.groupEnd();
			}
		)
	;
	// I am the recursive function, factored-out into its own function.
	function recurseToZero( n ) {
		console.log( "Entering recursive function for [", n, "]." );
		// Once we hit zero, bail out of the recursion. The key to recursion is that
		// it stops at some point, and the callstack can be "rolled" back up.
		if ( n === 0 ) {
			return( 0 );
		}
		// Start a NEW PROMISE CHAIN that will become the continuation of the parent
		// promise chain. This new promise chain now becomes a completely tangential
		// branch to the parent promise chain.
		var tangentialPromiseBranch = Promise.resolve().then(
			function() {
				return( recurseToZero( n - 1 ) ); // RECURSE!
			}
		);
		return( tangentialPromiseBranch );
	}
}

/*async-await*/

async function allPromises() {
	console.log("allPromises()");
	console.log("Try 1");
	try {
		console.log("await getPaper")
		let data = await getPaper.catch((err) => console.log('ERROR on getPaper'));
		console.log(data);


		console.log("Try 2");
		try {
			console.log("await writeLetter")
			data = await writeLetter.catch((err) => console.log('ERROR on writeLetter'));
			console.log(data);
		}
		catch (data) {
			console.log(data);
		}


		console.log("Try 3");
		try {
			console.log("await postLetter")
			data = await postLetter((res) => { console.log('postLetter success'); }, (err) => { console.log('ERROR on postLetter'); });
			console.log(data);
		}
		catch (data) {
			console.log(data);
		}
	}
	catch (data) {
		console.log(data);
	}


	console.log("await recurProm");
	await recurProm();

	console.log("END allPromises()");
}



allPromises();


/* Promise.all(), Promise.race() */

// Promise.all([ getPaper, writeLetter, postLetter])
//   .then(data => {
//     console.log("All done" + data);
//   })
//   .catch(data => {
//     console.log(data);
//   });

// Promise.race([writeLetter, getPaper, postLetter])
//   .then(data => {
//     console.log("All done" + data);
//   })
//   .catch(data => {
//     console.log(data);
//   });
