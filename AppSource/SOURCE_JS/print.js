
console.log("Print Package Loaded");

export default function printMe() {
	console.log('I get called from print.js! Bundled into a Dynamic Module Bundle (print_function.bundle.js), Async Loaded');
}
