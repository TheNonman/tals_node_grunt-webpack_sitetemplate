/* global Absurd */
'use strict';

const isDebugging = false;

function DebugFlowPrint ( string )
{
	if (!isDebugging)			return;
	console.log(string);
}


var absurd = absurd || Absurd();// Initialize AbsurdJS if it hasn't been already
var TalsContainedGlobalMenu = absurd.component('TalsContainedGlobalMenu', {
	links: [],
	content: null,
	position: null,
	isOpen: false,
	styleIt: function() {
		var x = this.position == 'right' ? (this.isOpen ? -380 : -80) : (this.isOpen ? 300 : 0);
		var leftMenu = this.position == 'right' ? '100%' : '10px';
		var leftMenuContent = this.position == 'right' ? '80px' : '-310px';
		this.css = {
			'#ContainedGlobalMenu': {
				position: 'absolute',
				top: 0,
				left: leftMenu,
				height: '100%',
				margin: '0 0 0 ' + x + 'px',
				transition: 'all 400ms',
				overflow: 'visible',
				'font-family': 'sans-serif',
				'font-size': '18px',
				'-webkit-transition': 'all 400ms',
				'-moz-transition': 'all 400ms',
				'-o-transition': 'all 400ms',
				'.toggle': {
					'text-decoration': 'none',
					'display': 'block',
					'text-align': 'center',
					margin: '20px 0 0 0',
					color: this.isOpen ? '#BDBDBD': '#6E6E6E',
					width: '60px',
					transition: 'all 400ms',
					'-webkit-transition': 'all 400ms',
					'-moz-transition': 'all 400ms',
					'-o-transition': 'all 400ms',
					'border-bottom': 'dotted 1px #FFF',
					'&:hover': { color: '#000', 'border-bottom': 'dotted 1px #333' }
				},
				'.menu-content': {
					position: 'absolute',
					top: 0,
					left: leftMenuContent,
					width: '300px',
					'background-color': '#565656',
					height: '100%',
					p: { padding: '20px', margin: 0, color: '#FFF', 'font-size': '30px' },
					a: {
						'display': 'block', color: '#FFF', 'text-decoration': 'none',
						'box-sizing': 'border-box',
						width: '100%', padding: '6px 20px',
						'border-top': 'dotted 1px #999', 'border-bottom': 'dotted 1px #333',
						transition: 'all 200ms',
						'-webkit-transition': 'all 200ms',
						'-moz-transition': 'all 200ms',
						'-o-transition': 'all 200ms',
						'&:hover': { padding: '6px 20px 6px 30px', 'background-color': '#848484' }
					}
				},
				'.menu-title': {
					p: {
						'font-family': 'serif',
						'font-size': '18px'
					}
				}
			}
		};
		this.css[this.content] = {
			transition: 'all 400ms',
			'-webkit-transition': 'all 400ms',
			'-moz-transition': 'all 400ms',
			'-o-transition': 'all 400ms'
		};
		if(this.isOpen) {
			this.css[this.content][this.position == 'left' ? 'padding-left' : 'padding-right'] = '320px';
		}
		if(this.isOpen) {
			this.css['#ContainedGlobalMenu']['.toggle'].animate = 'bounceInDown';
		}
		return this;
	},
	buildIt: function() {
		DebugFlowPrint("[TalsContainedGlobalMenu] :: buildIt");
		this.html = {
			'#ContainedGlobalMenu': {
				'button.toggle[data-absurd-event="click:toggle"]': this.isOpen ? 'Close' : 'Menu',
				'.menu-content': {
					'.menu-title': {
						p: this.title,
					},
					'.links': [
						'<% for(var i=0, link; i<links.length, link=links[i]; i++) { %>',
						{ 'a[href=<% link.url %>]': '<% link.label %>' },
						'<% } %>'
					]
				}
			}
		};
		return this;
	},
	toggle: function(e, dom) {
		DebugFlowPrint("[TalsContainedGlobalMenu] :: toggle");
		e.preventDefault();
		this.isOpen = !this.isOpen;
		this.styleIt().buildIt().populate();
	},
	ready: function(dom) {
		this
		.set('parent', dom('body').el)
		.styleIt().buildIt().populate();
	},
	constructor: function(links, content, position, title) {
		this.links = links;
		this.content = content;
		this.position = position;
		this.title = title;
	}
});

DebugFlowPrint("Get Attribute Properties from the Script Tag");
//DebugFlowPrint("TalsContainedGlobalMenu: " + TalsContainedGlobalMenu);
// Get Attribute Properties from the Script Tag
/*
var TalsContainedGlobalMenu__OffCanvas_Position = document
	.querySelector('script[data-absurd-script="TalsContainedGlobalMenu"][data-position]')
	.getAttribute('data-position');

var TalsContainedGlobalMenu_Title =  document
.querySelector('script[data-absurd-script="TalsContainedGlobalMenu"][data-menu-title]')
.getAttribute('data-menu-title');
*/

DebugFlowPrint("Init TalsContainedGlobalMenu");
//DebugFlowPrint("Initialize TalsContainedGlobalMenu with Title: " + TalsContainedGlobalMenu_Title);
//DebugFlowPrint("Initialize TalsContainedGlobalMenu to Position: " + TalsContainedGlobalMenu__OffCanvas_Position);

TalsContainedGlobalMenu([
	{ label: 'The Collection Root', url: '/'},
	{ label: 'JS Only Menus &amp; Content', url: '/JS_JS-Only-Menus'},
	{ label: 'Download', url: ''},
	{ label: 'Contribute', url: ''},
	{ label: 'Testing', url: ''}
], '.content', 'right', 'Title');
