
import jsonData from './test.json';
import xmlData from './test.xml';
import BaseImg from '../SOURCE_IMAGES/example.png';

if (process.env.NODE_ENV && process.env.NODE_ENV !== 'production') {
	console.log('index.js is Running under DEV Environment');
}

let elementBody = document.getElementsByTagName('body')[0];
let baseElement = document.getElementById('message_space');

const sampleImage = new Image();
sampleImage.src = BaseImg;
baseElement.insertBefore(sampleImage, baseElement.childNodes[0]);
