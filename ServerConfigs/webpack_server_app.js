var express = require('express');
var path = require('path');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');

var app = express();

let config = require('../BuildConfigs/WebPack/webpack.server.config.js');
config = Object.assign({ watch: true }, config);
const compiler = webpack(config);

// Define the port to run on
app.set('port', 3300);

app.use(webpackDevMiddleware(compiler, { publicPath: config.output.publicPath } ));
app.use(express.static(path.join(__dirname, '../_Staging')));

// Listen for requests
var server = app.listen(app.get('port'), function() {
  var port = server.address().port;
  console.log('Magic happens on port ' + port);
});
