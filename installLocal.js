// installLocal.js

/**
 * Script to run after npm install
 *
 * Copies all of the modules Scaffolding files to the project root.
 */

'use strict'

var fsOps = require('fs');
var pathOps = require('path');
var gentlyCopy = require('gently-copy');

var filesToCopy = [
	// Full Directories
	'AppSource', 'BuildConfigs', 'Documentation', 'ServerConfigs',
	// Individual Files
	'.editorconfig', 'gruntfile.js', 'jsconfig.json'];

// User's local, project directory
// Warning: This assumes the package is installed into '<User's local, project directory>/node_modules/<package-name>/`
var userPath = '../../'

var parentDirName = pathOps.basename(__dirname);
console.log("installLocal.js is in " + parentDirName);

// Verify that this Script is running from the NPM installed Directory Name: "tals_node_grunt_webpack_site_template"
// If so, copy all of the scaffolding files up to the project root.
if (parentDirName === "tals_node_grunt_webpack_site_template") {
	console.log("");
	console.log("Migrating Scaffolding Files...");
	console.log("");
	gentlyCopy(filesToCopy, userPath);

	// Rename the Project Git Ignore filename from 'project_gitignore' to '.gitignore'
	if (!fsOps.existsSync('../../.gitignore')) {
		gentlyCopy(['project_gitignore'], userPath);
		fsOps.rename('../../project_gitignore', '../../.gitignore', function(err) {
			if ( err ) console.log('RENAME ERROR: ' + err);
		});
	}

	// Rename the Project Git Attributes filename from 'project_gitattributes' to '.gitattributes'
	if (!fsOps.existsSync('../../.gitattributes')) {
		gentlyCopy(['project_gitattributes'], userPath);
		fsOps.rename('../../project_gitattributes', '../../.gitattributes', function(err) {
			if ( err ) console.log('RENAME ERROR: ' + err);
		});
	}

	// Rename the Project NPM Config filename from 'project_npmrc' to '.npmrc'
	if (!fsOps.existsSync('../../.npmrc')) {
		gentlyCopy(['project_npmrc'], userPath);
		fsOps.rename('../../project_npmrc', '../../.npmrc', function(err) {
			if ( err ) console.log('RENAME ERROR: ' + err);
		});
	}

	// Add a migrated, starting NPM package.json file if there isn't one.
	if (!fsOps.existsSync('../../package.json')) {
		gentlyCopy(['new-project-package.json'], userPath);
		fsOps.rename('../../new-project-package.json', '../../package.json', function(err) {
			if ( err ) console.log('RENAME ERROR: ' + err);
		});
	}

	// Add a migrated, starting ReadMe.md file if there isn't one.
	if (!fsOps.existsSync('../../ReadMe.md')) {
		gentlyCopy(['new-project-readme.md'], userPath);
		fsOps.rename('../../new-project-readme.md', '../../ReadMe.md', function(err) {
			if ( err ) console.log('RENAME ERROR: ' + err);
		});
	}
}
else {
	console.log("installLocal.js is NOT RUNNING FROM node_modules package: 'tals_node_grunt_webpack_site_template'");
	console.log("SKIPPING File Migration !!!");
}
